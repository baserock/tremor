#![feature(custom_derive, plugin)]
#![plugin(serde_macros)]

extern crate serde;
extern crate serde_yaml;

use std::cmp::Ordering;
use std::fs::File;
use serde_yaml::from_reader;
use serde::{Deserialize, Deserializer};

pub fn from_yaml_file<T, D> (d: &mut D) -> Result<T, D::Error> where T: Deserialize, D: Deserializer {
  let s: String = try!(Deserialize::deserialize(d));
  match from_reader(File::open(s).unwrap()) {
     Ok(something) => Ok(something),
     Err(err) => return Err(serde::de::Error::custom(err.to_string()))
  }
}

#[derive(PartialEq, Serialize, Deserialize, Debug, Default, Clone, Eq)]
pub struct BIT {
  #[serde(rename="configureCommands")]
  pub configure_commands : Option<Vec<String>>,
  #[serde(rename="buildCommands")]
  pub build_commands : Option<Vec<String>>,
  #[serde(rename="installCommands")]
  pub install_commands : Option<Vec<String>>
}

#[derive(PartialEq, Serialize, Deserialize, Debug, Default, Clone, Eq)]
pub struct Chunk {
  pub name : String,
  pub repo : String,
  #[serde(rename="ref")]
  pub reference : String,
  #[serde(rename="buildDepends")]
  pub build_depends : Option<Vec<String>>,
  #[serde(deserialize_with="from_yaml_file")]
  pub bit : BIT
}

#[derive(PartialEq, Serialize, Deserialize, Debug, Default, Clone, Eq)]
pub struct Stratum {
  pub name : String,
  #[serde(deserialize_with="from_yaml_file")]
  pub chunks : Vec<Chunk>,
  #[serde(rename="buildDepends")]
  pub build_depends : Option<Vec<String>>
}

#[derive(PartialEq, Serialize, Deserialize, Debug, Default, Clone, Eq)]
pub struct System {
  pub name : String,
  pub strata : Vec<Stratum>
}

#[test]
fn test_stratum_serde() {
  let x: Vec<Chunk> = from_reader(File::open("assets/test-stratum.morph").unwrap()).unwrap();
}

#[test]
fn test_system_serde() {
  let x : System = from_reader(File::open("assets/test-system.morph").unwrap()).unwrap();
}
